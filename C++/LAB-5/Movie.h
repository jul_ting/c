// Movie.h
#ifndef Movie_H
#define Movie_H
#include <string>

class Movie
{
  private:
    std::string title, director;
    int year;

  public:
    Movie();
    Movie(std::string _title);
    Movie(std::string _title, std::string _director, int _year);
    void setTitle(std::string _title);
    void setYear(int _year);
    void setDirector(std::string _director);
    void showAll();

};

#endif