#include <iostream>
#include <string>

using namespace std;

void pass_by_val(int &x, int &y)
{
	int temp = x;
	x = y;
	y = temp;
}
void pass_by_ref(int *x, int *y)
{
	cout << *x << endl;
	cout << x << endl;
	cout << &x << endl;
	int temp = *x;
	*x = *y;
	*y = temp;
	
}

void swapArray(int *array1, int *array2)
{
	int temp;
	// 						= 5
	for (int i = 0; i < (sizeof(array1)/sizeof(*array1)); i++)
	{
		temp = array1[i];
		array1[i] = array2[i];
		array2[i] = temp;
	};
}

int main(int argc, char *argv[])
{
	int x = 10, y = 20;
	// pass_by_val(x, y);
	cout << x << endl;
	cout << &x << endl;

	pass_by_ref(&x, &y);

	int intArray1[5] = {78, 25, 56, 84, 92};
	int intArray2[5] = {92, 57, 65, 92, 100};
	// swapArray(intArray1, intArray2);

	// for (int i = 0; i < 5; i++)
	// {
	// 	pass_by_ref(&intArray1[i], &intArray2[i]);
	// }

	// for (const int a : intArray1)
	// {
	// 	cout << a << endl;
	// }
	// cout << "\n"
	// 	 << endl;
	// for (const int a : intArray2)
	// {
	// 	cout << a << endl;
	// }

	return 0;
}