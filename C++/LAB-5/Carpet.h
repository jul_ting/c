#ifndef Carpet_H
#define Carpet_H

class Carpet
{
    int length;
    int width;

  public:
    Carpet(int _length, int _width);
    int calculateArea();
    void setLength(int _length);
    void setWidth(int _length);
    void printArea();
};

#endif