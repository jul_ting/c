#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

// Write an application using function that returns the smallest of three floatingpoint
// numbers
float returnSmallest(float a, float b, float c)
{
    float x;
    if (a < b && a < c)
    {
        x = a;
    }
    else if (b < c && b < a)
    {
        x = b;
    }
    else
    {
        x = c;
    }

    return x;
}

/*
An integer is said to be prime if it is divisible only by 1 and itself. For example, 2,
3, 5 and 7 are prime but 4, 6, 8 and 9 are not. Write a function that determines if a
number is prime 
*/
bool isPrime(int x)
{
    bool prime = true;
    if (x <= 0)
    {
        cout << "Out of bound" << endl;
        prime = false;
    }
    else
    {
        for (int i = 2; i <= x; i++)
        {
            if (x % i == 0)
            {
                if (i != x)
                {
                    cout << "divisible by " << i << endl;
                    prime = false;
                    break;
                }
            }
        }
    }
    return prime;
}

/*
Write a function that takes an integer value and returns the number with its digits
reversed. For example, given number 7631, the function should return 1367.
*/
int intVerter(int x)
{
    string xString = to_string(x);
    int n = xString.length();
    for (int i = 0; i < n / 2; i++)
    {
        swap(xString[i], xString[n - i - 1]);
    }
    x = stoi(xString);
    return x;
}
/*
Write an application with the function qualityPoints that inputs a student’s
average and returns 4 if a student’s average is 90 – 100, 3 if the average is 80 –
89, 2 if the average us 70 – 79, 1 if the average is 60 – 69 and 0 if the average is
lower than 60. 
*/

int qualityPoints(float average)
{
    int x = 0;
    if (average >= 0 && average <= 100)
    {
        if (average >= 90 && average <= 100)
        {
            x = 4;
        }
        else if (average >= 80 && average <= 89)
        {
            x = 3;
        }
        else if (average >= 70 && average <= 79)
        {
            x = 2;
        }
        else if (average >= 60 && average <= 69)
        {
            x = 1;
        }
        else
        {
            x = 0;
        }
    }

    return x;
}

/*

Write a program which accepts 3 item prices from the user. It then calls a function
which calculates the net price of each of the 3 prices after deducting the discount
offered on the items. Below is the discount offered for each of the items. Display
the price after discount on the output screen.
Item Discount
Item 1 20%
Item 2 30%
Item 3 40% 
*/
void calculateDiscount(double a, double b, double c)
{
    cout << "Net price of Item 1 is " << a * 0.8 << endl;
    cout << "Net price of Item 2 is " << a * 0.7 << endl;
    cout << "Net price of Item 3 is " << a * 0.6 << endl;
}

/*

Define a class named Movie. Include private data members title, year, and name of
the director. Include three public functions with prototypes void setTitle(string
movieTitle); void setYear(int year); and void setDirector(string dir);. Include
another function that displays information about a movie. Write a main( ) function
that declares a movie object named myFavouriteMovie. Set and display the object’s
data members
*/

class Movie
{
  public:
    void setTitle(string movieTitle)
    {
        title = movieTitle;
    }
    void setYear(int year)
    {
        yearOfMovie = year;
    }
    void setDirector(string dir)
    {
        nameOfDir = dir;
    }
    void showAll()
    {
        cout << "Title = " << title << " Year = " << yearOfMovie << " Name of Director = " << nameOfDir << endl;
    }

  private:
    string title, nameOfDir;
    int yearOfMovie;
};

void mySwap(int &xRef, int &yRef)
{
    int temp = xRef;
    xRef = yRef;
    yRef = temp;
}

int main(int argc, char *argv[])
{
    /* show of my swapping method using reference */
    // int a = 1, b = 2;
    // mySwap(a, b);
    // cout << "a is " << a << endl;
    // cout << "b is " << b << endl;

    /* return the smallest of the 3 numbers */
    // float smallest = returnSmallest(12, 10.2, 10.3);
    // cout << "Smallest value is " << smallest << endl;

    /* find a prime number here */
    int a;
    while (true)
    {
        cout << "Type here: " << endl;
        cin >> a;
        if (isPrime(a))
        {
            cout << "I found a prime!" << endl;
        }
    }

    /* invert the digits and return as int */
    // cout << intVerter(15234) << endl;

    /* quality points system */
    // int x = qualityPoints(80);
    // cout << "Quality points = " << x << endl;

    /* calculate discount */
    // calculateDiscount(100,200,300);

    // Movie movie;
    // movie.setDirector("JK Bowling");
    // movie.setTitle("Harry Pottah");
    // movie.setYear(2000);
    // movie.showAll();

    return 0;
}