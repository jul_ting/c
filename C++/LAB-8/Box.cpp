#include <iostream>
#include <string>

using namespace std;
class Box
{
  private:
    double height, breadth, length;

  public:
    void setLength(double _height)
    {
        length = _height;
    };

    double getLength()
    {
        return length;
    };
    void setBreath(double _breadth)
    {
        breadth = _breadth;
    };

    double getBreath()
    {
        return breadth;
    };
    void setHeight(double _height)
    {
        height = _height;
    };

    double getHeight()
    {
        return height;
    };
    double getVolume()
    {
        return height * breadth * length;
    };
    Box operator+(const Box &b)
    {
        Box temp;
        temp.height = this->height + b.height;
        temp.breadth = this->breadth + b.breadth;
        temp.length = this->length + b.length;
        return temp;
    }
    Box(double _height, double _breadth, double _length)
    {
        height = _height;
        breadth = _breadth;
        length = _length;
    }
    Box() {}
};
int main(int argc, char *argv[])
{
    Box b1(3, 4, 5), b2(2, 2, 2);
    cout << b1.getVolume() << endl;
    cout << b2.getVolume() << endl;
    Box b3 = b1 + b2;
    cout << b3.getVolume() << endl;
}