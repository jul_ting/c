#include <iostream>
#include <string>

using namespace std;
class Distance
{
  public:
    int feet, inch;
    Distance()
    {
        feet = 0;
        inch = 0;
    }
    Distance operator-()
    {
        Distance temp;
        temp.feet = -feet;
        temp.inch = -inch;
        return temp;
    };
    bool operator<(const Distance &d)
    {
        if (this->feet < d.feet)
        {
            return true;
        }
        else if (this->feet == d.feet && this->inch < d.inch)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool operator==(const Distance &d)
    {
        if (this->feet == d.feet && this->inch == d.inch)
            return true;
        return false;
    }
};
int main(int argc, char *argv[])
{
    Distance d1;
    cout << d1.feet << "\t" << d1.inch << endl;
    d1.feet = 20;
    d1.inch = 30;
    cout << d1.feet << "\t" << d1.inch << endl;
    Distance d2;
    d2.feet = 20;
    d2.inch = 30;
    cout << d2.feet << "\t" << d2.inch << endl;

    if (d1 < d2)
    {
        cout << "d1 < d2" << endl;
    }
    else
    {
        cout << "d1 not < d2" << endl;
    }

    // if (d1 == d2)
    //     cout << "true" << endl;
    // cout << "false" << endl;
    return 0;
}