#include <iostream>
#include <string>

using namespace std;

/*          Dynamic casting with pointers and class             */
class A
{
public:
  virtual void funcA() {}
};
class B : public A
{
public:
  virtual void funcB() {}
};
class C : public A
{
public:
  virtual void funcC() {}
};

int main(int argc, char *argv[])
{
  /*          Static casting             */
  // int a = 31;
  // int b = 3;
  // float x = a / b;
  // float y = static_cast<float>(a) / b;
  // system("clear");
  // cout << "Output without static_cast = " << x << endl;
  // cout << "Output with static_cast = " << y << endl;

  /*          Pointers          */
  // int i = 0;
  // int *x = &i;
  // *x = 10;
  // cout << to_string(i) << endl;
  // cout << *x << endl;

  /*          Dynamic casting with pointers and class             */
  // A *A_Array[5];
  // B *BObj = new B;
  // C *CObj = new C;
  // A_Array[0] = BObj;
  // A_Array[1] = CObj;
  // A_Array[2] = CObj;
  // A_Array[3] = BObj;
  // for (int i = 0; i < 4; i++)
  // {
  //     if (dynamic_cast<B *>(A_Array[i]))
  //     {
  //         cout << "B Type" << endl;
  //     }
  //     if (dynamic_cast<C *>(A_Array[i]))
  //     {
  //         cout << "C Type" << endl;
  //     }
  // }

  /*  swapping value  */
  int array[5], temp[5], x;
  array[0] = 1;
  array[1] = 2;
  array[2] = 3;
  array[3] = 4;
  array[4] = 5;
  temp[0] = 100;
  temp[1] = 200;
  temp[2] = 300;
  temp[3] = 400;
  temp[4] = 500;
  //cout << (sizeof(array)/sizeof(*array)) << endl;
  for (int i = 0; i < (sizeof(array) / sizeof(*array)); i++)
  {
    cout << "Location of array = " << &array[i] << " is " << array[i] << "	Location of temp = " << &temp[i] << " is " << temp[i] << endl;
  }

  cout << "\n" << endl;

  for (int i = 0; i < (sizeof(array) / sizeof(*array)); i++)
  {

    /* choose one of the two methods below */
    // swap(array[i], temp[i]);

    x = array[i];
    array[i] = temp[i];
    temp[i] = x;

    cout << "Location of array = " << &array[i] << " is " << array[i] << "	 Location of temp = " << &temp[i] << " is " << temp[i] << endl;
  }
  return 0;
}

/*
Use references
In function parameters and return types.
Use pointers:
To implement data structures like linked list, tree, etc and their algorithms.
Use pointers if  pointer arithmetic or passing NULL-pointer is needed. For example for arrays (Note that array access is implemented using pointer arithmetic).
*/