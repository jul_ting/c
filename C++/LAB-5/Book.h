// Book.h
#ifndef Book_H
#define Book_H
#include <string>

// Create a class to store book details such as bookID, pages, title, publisher, price and
// copies. The class contains the following functions
// a. addBook which assigns values to all the details above
// b. get functions that returns the values of each of the book details separately
// c. displayBook which displays the bookID and title of the book using the
// appropriate get functions


class Book
{
  private:
    std::string bookID, title, publisher;
    int pages, copies;
    float price;

  public:
    Book();
    void addBook(std::string _bookID, std::string _title, std::string _publisher, int _pages, int _copies, float _price);
    void displayBook();

    std::string getbookID();
    std::string getTitle();
    std::string getPublisher();
    int getPages();
    int getCopies();
    float getPrice();

};

#endif