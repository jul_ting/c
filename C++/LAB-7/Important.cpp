#include <iostream>
#include <string>

using namespace std;

class Staff
{
  private:
    double salary;
    string name;

  public:
    Staff(){};
    Staff(string _name) : name(_name){};
    void setSalary(double _salary) { salary = _salary; };
    double getSalary() { return salary; };
    string getName() { return name; };
    virtual void salCal(){};
    virtual void displaySal(){};
};
class Executive : public Staff
{
  private:
    double basicPay, allowance, deduction;

  public:
    Executive(string _name, double _basicPay, double _allowance, double _deduction) : Staff(_name), basicPay(_basicPay), allowance(_allowance), deduction(_deduction){};

    void salCal()
    {
        setSalary(basicPay + allowance - deduction);
    };
    void displaySal()
    {
        cout << "Name = " << getName() << endl;
        cout << "basicPay = " << basicPay << endl;
        cout << "allowance = " << allowance << endl;
        cout << "deduction = " << deduction << endl;
        cout << "Salary = " << getSalary() << endl;
    };
};
class nonExecutive : public Staff
{
  public:
    nonExecutive(string _name, double _dailyWage, double _noOfDays) : Staff(_name), dailyWage(_dailyWage), noOfDays(_noOfDays){};

    void salCal()
    {
        setSalary(dailyWage * noOfDays);
    };
    void displaySal()
    {
        cout << "Name = " << getName() << endl;
        cout << "dailyWage = " << dailyWage << endl;
        cout << "Number Of Days worked = " << noOfDays << endl;
        cout << "Salary = " << getSalary() << endl;
    };

  private:
    double dailyWage, noOfDays;
};

int main(int argc, char *argv[])
{
    Staff staff("somebody");
    cout << staff.getName() << endl;

    Executive exec("Executive someone", 300, 100, 50);
    exec.salCal();
    exec.displaySal();

    nonExecutive nonExec("Non Exec", 50, 2);
    nonExec.salCal();
    nonExec.displaySal();

    system("clear");

    Staff *staffArray[2];
    staffArray[0] = &exec;
    staffArray[1] = &nonExec;

    for (int i = 0; i < 2; i++)
    {
        staffArray[i]->salCal();
        staffArray[i]->displaySal();
    }

    // Staff sArray[2];
    // sArray[0] = exec;
    // sArray[1] = nonExec;
    // for (int i = 0; i < 2; i++)
    // {
    //     sArray[i].displaySal();
    // }
    return 0;
}