#include <iostream>
#include <string>
#include "Carpet.h"

// questions at file:///Users/juliusting/Downloads/Lab-Classes_Objects.pdf
using namespace std;

Carpet::Carpet(int _length, int _width)
{
    length = _length;
    width = _width;
}

int Carpet::calculateArea()
{
    return length * width;
}

void Carpet::setLength(int _length)
{
    length = _length;
}

void Carpet::setWidth(int _width)
{
    width = _width;
};

void Carpet::printArea()
{
    cout << "The Area of the carpet is " << length * width << " feet." << endl;
};

int main(int argc, char *argv[])
{
    Carpet carpet1(10, 12);
    Carpet carpet2(4, 6);
    Carpet carpet3(8, 12);
    Carpet carpet4(12, 8);
    carpet1.printArea();
    carpet2.printArea();
    carpet3.printArea();
    carpet4.printArea();

    
    carpet1.setLength(1);
    carpet1.setWidth(1);
    carpet1.printArea();
    return 0;
}