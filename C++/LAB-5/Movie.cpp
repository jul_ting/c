#include <iostream>
#include <string>
#include "Movie.h"

using namespace std;

// Define a class named Movie. Include private fields for the title, year, and name of the
// director. Include three public functions with prototypes void setMovie(string
// _title, int year, string dir); Include another function that displays information
// about a movie. Give this function an appropriate name. Write a main( ) function that
// declares a movie object named myFavouriteMovie. Set the object’s field. Use the
// other function to display the values in the fields.

Movie::Movie(){};
Movie::Movie(string _title)
{
    title = _title;
};
Movie::Movie(string _title, string _director, int _year)
{
    title = _title;
    director = _director;
    year = _year;
};

void Movie::setTitle(string _title)
{
    title = _title;
};
void Movie::setYear(int _year)
{
    year = _year;
};
void Movie::setDirector(string _director)
{
    director = _director;
};
void Movie::showAll()
{
    cout << "Title = " << title << "\nYear = " << year << "\nName of Director = " << director << endl;
};

int main(int argc, char *argv[])
{
    Movie movie1("The title");
    movie1.setYear(1995);
    movie1.setDirector("Maxie");
    movie1.showAll();
    Movie movie2("I don't have any idea", "Kelvin", 2000);
    movie2.showAll();
    return 0;
}