#include <iostream>
#include <string>

using namespace std;

void additionOf1toX()
{
	int sum = 0, i = 1, num;
	cout << "addition of positive integer from 1 to... " << endl;
	cin >> num;

	while (i <= num)
	{
		sum += i;
		i++;
	}
	cout << sum << endl;
}

void factorial()
{
	unsigned int total = 1, num, i = 1;
	cout << "Factorial of integer..." << endl;
	cin >> num;
	while (i <= num)
	{
		total *= num;
		num--;
	}
	cout << total << endl;
}
void fibonacci()
{
	unsigned long int a = 0, b = 1, c;
	int num;
	cout << "display first _ numbers of fibonacci" << endl;
	cin >> num;
	if (num >= 1)
	{
		cout << a << endl;
		num--;
	}
	if (num >= 1)
	{
		cout << b << endl;
		num--;
	}
	while (num > 0)
	{
		c = a + b;
		a = b;
		b = c;
		cout << c << endl;
		num--;
	}
}

int main(int argc, char *argv[])
{
	fibonacci();
	return 0;
}