#include <iostream>
#include <string>
#include "Account.h"

using namespace std;

Account::Account(){};
Account::Account(std::string _id, std::string _name)
{
    id = _id;
    name = _name;
};
Account::Account(std::string _id, std::string _name, double _balance)
{
    id = _id;
    name = _name;
    balance = _balance;
};
void Account::deposit(double amount)
{
    if (amount < 0)
    {
        cout << "You cannot deposit less than 0" << endl;
    }
    else
    {
        cout << "Depositing RM " << amount << endl;
        balance += amount;
        cout << "Balance = RM " << balance << endl;
    }
};
void Account::withdraw(double amount)
{
    if (amount < 0)
    {
        cout << "You cannot withdraw less than 0" << endl;
    }
    else if (amount > balance)
    {
        cout << "You cannot withdraw more than your balance" << endl;
    }
    else
    {
        cout << "Dispensing RM " << amount << endl;
        balance -= amount;
        cout << "Balance = RM " << balance << endl;
    }
};

void Account::printDetails()
{
    cout << "ID = " << id << "\nName = " << name << "\nBalance = RM " << balance << endl;
}
int main(int argc, char *argv[])
{
    // string id, name;

    // double balance;
    // cout << "What is your id?" << endl;
    // cin >> id;
    // getline(cin, name);
    // cout << "What is your name?" << endl;
    // getline(cin, name);
    // cout << "What is your balance?" << endl;
    // cin >> balance;

    // double _w, _d;
    // Account heh(id, name, balance);
    // cout << "How much you wanna withdraw?" << endl;
    // cin >> _w;
    // heh.withdraw(_w);
    // heh.printDetails();
    // cout << "How much you wanna deposit?" << endl;
    // cin >> _d;
    // heh.deposit(_d);
    // heh.printDetails();

    // string _branchName;
    // Account _deh("hehID", "heh", 2000);

    int *x;
    int y = 10;
    x = &y;
    cout << x << endl;
    cout << *x << endl;
    return 0;
}