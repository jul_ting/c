#include <iostream>
#include <string>
#include <iostream>
#define MAX 5

void push();
void pop();
void display();
int top = -1;
int stack_arr[MAX];

using namespace std;

int main(int argc, char *argv[])
{

	int ch;
	system("CLS");
	cout << "\n\t\t"
		 << "Stack Operation" << endl;
	cout << "\n\t\t"
		 << "............................." << endl;
	while (1)
	{
		cout << "\n*************************************" << endl;
		cout << "\n1. Push" << endl;
		cout << "\n2. Pop" << endl;
		cout << "\n3. Display" << endl;
		cout << "\n4. Exit" << endl;
		cout << "\n*************************************" << endl;
		cout << "\nEnter ur choice: ";
		cin >> ch;
		switch (ch)
		{
		case 1:
			push();
			break;
		case 2:
			pop();
			break;
		case 3:
			display();
			break;
		case 4:
			exit(0);
		default:
			cout << "Invalid Operation" << endl;
		}
	}

	return 0;
}

void pop()
{
	if (top == -1)
		cout << "\tStack underflow" << endl;
	else
	{
		cout << "\nPopped element is: " << stack_arr[top];
		top -= 1;
	}
}
void push()
{
	int pitem;
	if (top == (MAX - 1))
		cout << "\tStack overflow" << endl;
	else
	{
		cout << "Enter the item to be pushed in stack: " << endl;
		cin >> pitem;
		top += 1;
		stack_arr[top] = pitem;
	}
}

void display()
{
	if (top == -1)
		cout << "\nStack is empty" << endl;
	else
	{
		cout << "\nStack element" << endl;
		for (int x = top; x >= 0; x--)
		{
			cout << stack_arr[x] << "\t";
		}
		cout << endl;
	}
}