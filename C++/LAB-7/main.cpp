#include <iostream>
#include <string>

using namespace std;

class One
{
  public:
	int someInt;
	void Scream()
	{
		cout << "I am onichan" << endl;
	}
};
class Two : public One
{
  public:
	void Scream()
	{
		cout << "I am two chan" << endl;
	}
};

int main(int argc, char *argv[])
{
	One satu;
	Two dua;

	One *Numbers[5];
	Numbers[0] = &satu;
	Numbers[1] = &dua;
	satu.someInt = 1;
	dua.someInt = 2;

	// satu.Scream();
	// I am onichan
	// dua.Scream();
	// I am two chan

	// One *one;
	// one->Scream();
	// I am onichan

	// satu = dua;
	// satu.Scream();
	// I am onichan

	// satu.someInt = 1;
	// dua.someInt = 2;
	// satu = dua; // this takes all the public variables from dua and put it into satu

	// cout << satu.someInt << endl;
	// satu.Scream();
	for (int i = 0; i < 2; i++)
	{
		cout << Numbers[i]->someInt << endl;
	}
	return 0;
}
