#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
	int row = 10, column = 10;
	int x[row][column];
	int y[5][5];
	int k = 1;
	for (int i = 0; i < row; i++)
	{
		cout << "ROW " << i << "|\t";
		for (int j = 0; j < column; j++)
		{
			x[i][j] = k;
			cout << x[i][j] << "\t";
			k++;
		}
		cout << endl;
	}
	cout << "####\tdone\t####" << endl;
	for (int i = 0; i < row; i++)
	{
		cout << "ROW " << i << "|\t";
		for (int j = 0; j < column; j++)
		{
			swap(x[i][j], x[j][i]);
			cout << x[i][j] << "\t";
		}
		cout << endl;
	}
	return 0;
}