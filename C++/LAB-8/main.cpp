#include <iostream>
#include <string>

using namespace std;
class Counter
{
  private:
	int count;

  public:
	void setCount(int _count)
	{
		count = _count;
	};

	int getCount()
	{
		return count;
	};
	Counter() { count = 0; }
	Counter(int _count) { count = _count; }
	Counter operator++()
	{
		Counter temp;
		temp.count = ++count;
		return temp;
	};
	Counter operator--()
	{
		Counter temp;
		temp.count = --count;
		return temp;
	};
};
int main(int argc, char *argv[])
{
	Counter c1;
	Counter c2 = ++c1;
	cout << c1.getCount() << endl;
	cout << c2.getCount() << endl;
	--c1;
	cout << c1.getCount() << endl;
}