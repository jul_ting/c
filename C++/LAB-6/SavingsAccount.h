// SavingsAccount.h
#ifndef SavingsAccount_H
#define SavingsAccount_H
#include <string>
#include "Account.h"

class SavingsAccount : public Account
{
  private:
    std::string branchCode, accName;
    double  minBalance, transferFee;

  public:
    SavingsAccount();
    SavingsAccount(std::string _branchName);
    SavingsAccount(std::string, std::string, double, double);
    void deposit(float amount);
    void setBranchName(std::string _BranchName);
    std::string getBranchName();
};

#endif