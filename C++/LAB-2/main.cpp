#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
    int a = 1, b = 2, temp;
    cout << "a = " << a << " b = " << b << endl;
    swap(a,b);
    cout << "a = " << a << " b = " << b << endl;
    temp = a;
    a = b;
    b = temp;
    cout << "a = " << a << " b = " << b << endl;

}