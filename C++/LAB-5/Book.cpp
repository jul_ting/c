#include <iostream>
#include <string>
#include "Book.h"

using namespace std;

// Create the main function where an array of five book objects is created. Use a forloop
// to assign values to all the 5 book objects. Use the displayBook function within
// the for-loop to display both the bookID and title of each book in a listing. Prompt the
// user to determine which book details they want to view. Based on the user input, use
// the selection control to select the book from the array of books to display all details
// stored in the book object.

Book::Book(){};
void Book::addBook(string _bookID, string _title, string _publisher, int _pages, int _copies, float _price)
{
    bookID = _bookID;
    title = _title;
    publisher = _publisher;
    pages = _pages;
    copies = _copies;
    price = _price;
};

void Book::displayBook()
{
    cout << "Book ID: " << bookID << endl;
    cout << "Title: " << title << endl;
    cout << "Publisher: " << publisher << endl;
    cout << "Pages: " << pages << endl;
    cout << "Copies in store: " << copies << endl;
    cout << "Price: RM" << price << endl;
};
string Book::getbookID()
{
    return bookID;
};
string Book::getTitle()
{
    return title;
};
string Book::getPublisher()
{
    return publisher;
};
int Book::getPages()
{
    return pages;
};
int Book::getCopies()
{
    return copies;
};
float Book::getPrice()
{
    return price;
};

int main(int argc, char *argv[])
{
    Book books[5];
    for (int i = 0; i < 5; i++)
    {
        books[i].addBook(
            "ID10" + to_string(i),
            "Title 10" + to_string(i),
            "Publisher 10" + to_string(i),
            300 + i,
            15 + i,
            20 + i);
        books[i].displayBook();
    }
    // book1.addBook("ID101", "Title 101", "publisher 101", 300, 20, 200.00);
    return 0;
}