
// Exercise: Queue Operation Using Linked List
// Write a c++ program for creation queue and its operation using linked list.
// PROGRAM CODING:
#include <iostream>
using namespace std;
struct node
{
    int data;
    struct node *link;
};
class queue
{
    struct node *front, *rear, *ptr;

  public:
    queue()
    {
        rear = NULL;
        front = NULL;
    }
    void enqueue(int);
    void dqueue();
    void display();
};
void queue::enqueue(int data)
{
    ptr = new node;
    if (rear == NULL)
    {
        front = ptr;
    }
    else
    {
        rear->link = ptr;
        ptr->link = NULL;
        cout << rear << endl;
    }
    rear = ptr;
    rear->data = data;
    cout << "recorded." << endl;
}
void queue::dqueue()
{
    int item;
    if (front == NULL)
        cout << "Queue is empty:" << endl;
    else
    {
        item = front->data;
        if (front == rear)
        {
            front = NULL;
            rear = NULL;
        }
        else
        {
            ptr = front;
            front = front->link;
            delete ptr;
        }
        cout << "Deleted element : " << item << endl;
    }
}
void queue::display()
{
    ptr = front;
    if (ptr == NULL)
        cout << "Queue is empty" << endl;
    else
    {
        cout << "Elements in queue are: " << endl;
        while (ptr != NULL)
        {
            cout << ptr->data << " | ";
            ptr = ptr->link;
        }
        cout << endl;
    }
}
int main()
{
    queue q;
    int ch, data;
    while (1)
    {
        cout << "........MENU.........." << endl;
        cout << "1. enqueue" << endl;
        cout << "2. dqueue" << endl;
        cout << "3. display" << endl;
        cout << "4. exit" << endl;
        cout << "Enter your choice: " << endl;
        cin >> ch;
        switch (ch)
        {
        case 1:
            cout << "Enter the element: ";
            cin >> data;
            q.enqueue(data);
            break;
        case 2:
            q.dqueue();
            break;
        case 3:
            q.display();
            break;
        case 4:
            ch = 5;
            break;
        default:
            cout << "Invalid selection" << endl;
            break;
        }
        if (ch == 5)
            break;
    }
    return 0;
}