// DemoBasePerson.h
#ifndef DemoBasePerson_H
#define DemoBasePerson_H
#include <string>

class DemoBasePerson
{
  private:
    double salary;

  protected:
    int age;

  public:
    char initial;
    DemoBasePerson();
    void setPerson(double, int, char);
    void showPerson();
};

#endif