// Account.h
#ifndef Account_H
#define Account_H
#include <string>

class Account
{
private:
  std::string id, name;
  double balance;

protected:
  std::string branchName;

public:
  Account();
  Account(std::string _id, std::string _name);
  Account(std::string _id, std::string _name, double _balance);
  void deposit(double amount);
  void withdraw(double amount);
  void printDetails();
  double getBalance();
  void setBalance(double _balance);
  
};

#endif