#include <iostream>
#include <string>
#include "SavingsAccount.h"
#include "Account.h"

using namespace std;

SavingsAccount::SavingsAccount(){};
SavingsAccount::SavingsAccount(string _branchName)
{
    branchName = _branchName;
};
SavingsAccount::SavingsAccount(string _branchCode, string _accName, double _minBalance, double _transferFee)
{
    branchCode = _branchCode;
    accName = _accName;
    minBalance = _minBalance;
    transferFee = _transferFee;
};

Account::Account(){};
void SavingsAccount::deposit(float amount)
{
    if (amount < 0)
    {
        cout << "You cannot deposit less than 0" << endl;
    }
    else
    {
        cout << "Depositing RM " << amount << endl;
        // balance += amount;
        // cout << "Balance = RM " << balance << endl;
    }
};
void Account::printDetails(){};
void SavingsAccount::setBranchName(string _BranchName)
{
    branchName = _BranchName;
};

string SavingsAccount::getBranchName()
{
    return branchName;
};

int main(int argc, char *argv[])
{
    // SavingsAccount sa("BC202", "Savings", 50, 0.30);
    // sa.withdraw(50);
    // sa.deposit(100);
    // sa.printDetails();
    SavingsAccount sa("branch name");
    cout << sa.getBranchName() << endl;
    return 0;
}