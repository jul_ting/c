#include <iostream>
#include <string>
#include "DemoBasePerson.h"
#include "Extrovert.h"

using namespace std;

DemoBasePerson::DemoBasePerson(){};
void DemoBasePerson::setPerson(double _salary, int _age, char _initial)
{
    salary = _salary;
    age = _age;
    initial = _initial;
};
void DemoBasePerson::showPerson()
{
    cout << salary << age << initial << endl;
};

Extrovert::Extrovert(){};
void Extrovert::setExtro(string _favSports, double _expenditure, int _noOfEventsPerYear)
{
    favSports = _favSports;
    expenditure = _expenditure;
    noOfEventsPerYear = _noOfEventsPerYear;
};

void Extrovert::showExtrovert()
{
    cout << favSports << expenditure << noOfEventsPerYear << endl;
};

int main(int argc, char *argv[])
{
    // DemoBasePerson someone;
    // someone.setPerson(2000, 20, 'C');
    // someone.showPerson();

    Extrovert noone;
    noone.setExtro("games", 800, 2);
    noone.setPerson(2000, 20, 'C');
    noone.showPerson();
    noone.showExtrovert();
    return 0;
}