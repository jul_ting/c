#ifndef Extrovert_H
#define Extrovert_H
#include <string>
#include "DemoBasePerson.h"

class Extrovert : public DemoBasePerson
{
private:
  std::string favSports;

protected:
  double expenditure;

public:
  Extrovert();
  int noOfEventsPerYear;
  void setExtro(std::string _favSports, double _expenditure, int _noOfEventsPerYear);
  void showExtrovert();
};

#endif